const User = require("../models/user");

exports.getLogin = (req, res, next) => {
  res.render("auth/login", {
    pageTitle: "Login",
    path: "/login"
  });
};

exports.postLogin = (req, res, next) => {
  const { password, email } = req.body;

  User.findAll({ where: { email } })
    .then(user => {
      if (user) {
        const error = new Error("User already exists!");

        throw error;
      }

      localStorage.setItem("auth", true);
      res.redirect("/");
    })
    .catch(err => {
      console.log(err);
    });
};
