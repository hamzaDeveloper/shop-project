const Sequelize = require("sequelize");

const sequelize = new Sequelize("shop", "root", "hamzahamza1", {
  host: "localhost",
  dialect: "mysql"
});

// test the connection established or not
sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully ");
  })
  .catch(err => console.log("Unable to connect to the database error: ", err));

module.exports = sequelize;
